<?php

namespace Nyuk;

class Constants
{
    /**
     * @return int
     */
    static public function platformsCategoryID()
    {
        return defined('DOUG_PLATFORMS_CATEGORY') ? DOUG_PLATFORMS_CATEGORY : 14;
    }

    /**
     * @return int
     */
    static public function topicsCategoryID() {
        return defined('DOUG_TOPICS_CATEGORY') ? DOUG_TOPICS_CATEGORY : 17;
    }
}
