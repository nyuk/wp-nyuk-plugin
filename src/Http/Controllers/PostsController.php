<?php

namespace Nyuk\Http\Controllers;

use Nyuk\Helpers\PostHelper;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

class PostsController
{
    public function __construct()
    {
        register_rest_route('nyuk/v1', '/posts', array(
            array(
                'methods' => \WP_REST_Server::READABLE,
                'callback' => array($this, 'get_posts'),
                'permission_callback' => array($this, 'get_posts_permissions_check'),
            ),
        ));

        register_rest_route('nyuk/v1', 'posts/(?P<name>[a-zA-Z0-9-_]+)', array(
            array(
                'methods' => \WP_REST_Server::READABLE,
                'callback' => array($this, 'get_post'),
                'permission_callback' => array($this, 'get_post_permissions_check'),
            ),
        ));
    }

    /**
     * @param  WP_REST_Request  $request  Full data about the request.
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_posts($request)
    {
        $page = $this->getPageFromRequest($request);
        $perPage = (int) get_option('posts_per_page') ? (int) get_option('posts_per_page') : 10;
        $posts = [];

        $args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'numberposts' => $perPage,
            'offset' => ($page - 1) * $perPage,
        ];

        $rawPosts = get_posts($args);

        foreach ($rawPosts as $rawPost) {
            $postHelper = new PostHelper($rawPost);
            $posts[] = $postHelper->simple();
        }

        return new WP_REST_Response($posts, 200);
    }

    /**
     * @param  WP_REST_Request  $request  Full data about the request.
     *
     * @return WP_Error|bool
     */
    public function get_posts_permissions_check($request)
    {
        return true;
    }

    /**
     * @param  WP_REST_Request  $request  Full data about the request.
     *
     * @return WP_Error|WP_REST_Response
     */
    public function get_post($request)
    {
        $args = [
            'name' => $request->get_param('name'),
            'post_type' => 'post',
            'post_status' => 'publish',
            'numberposts' => 1,
        ];

        $rawPosts = get_posts($args);
        if (count($rawPosts) !== 1) {
            return new WP_REST_Response(null, 404);
        }

        $rawPost = array_pop($rawPosts);

        $postHelper = new PostHelper($rawPost);
        $post = $postHelper->simple();
        $post['post_content'] = apply_filters('the_content', $rawPost->post_content);

        return new WP_REST_Response($post, 200);
    }

    /**
     * @param  WP_REST_Request  $request  Full data about the request.
     *
     * @return WP_Error|bool
     */
    public function get_post_permissions_check($request)
    {
        return true;
    }

    /**
     * @param  WP_REST_Request  $request  Full data about the request.
     * @return int
     */
    private function getPageFromRequest($request)
    {
        $params = $request->get_query_params();

        if (isset($params['page']) !== true) {
            return 1;
        }

        if ((int) $params['page'] < 1) {
            return 1;
        }

        return (int) $params['page'];
    }
}
