<?php

use Nyuk\Helpers\FirestoreHelper;

add_action('delete_post', 'nyuk_delete_post');

function nyuk_delete_post($id)
{
    $firestoreHelper = new FirestoreHelper();

    $response = $firestoreHelper->deleteDocument('posts', $id);
    if ($response->getStatusCode() !== 200) {
        ddd((string) $response->getBody());
    }
}
