<?php

use Nyuk\Http\Controllers\PostsController;

add_action('rest_api_init', function () {
    new PostsController();
});
