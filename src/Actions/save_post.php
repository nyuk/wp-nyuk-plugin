<?php

use Nyuk\Helpers\FirestoreHelper;
use Nyuk\Helpers\PostHelper;

add_action('save_post', 'nyuk_save_post');

function nyuk_save_post($id)
{
    $rawPost = get_post($id);
    if ($rawPost == null) {
        return;
    }

    if ($rawPost->post_parent != 0) {
        $rawPost = get_post($rawPost->post_parent);
    }

    if ($rawPost == null) {
        return;
    }

    $firestoreHelper = new FirestoreHelper();
    $postHelper = new PostHelper($rawPost);

    $response = $firestoreHelper->setDocument('posts', $rawPost->ID, $postHelper->simple());
    if ($response->getStatusCode() !== 200) {
        ddd((string) $response->getBody());
    }

    $response = $firestoreHelper->setDocument('post_details', $rawPost->ID, $postHelper->withDetails());
    if ($response->getStatusCode() !== 200) {
        ddd((string) $response->getBody());
    }
}
