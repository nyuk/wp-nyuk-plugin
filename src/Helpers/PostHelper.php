<?php

namespace Nyuk\Helpers;

use Nyuk\Constants;
use WP_Post;
use WP_Term;

class PostHelper
{
    /** @var WP_Post */
    private $rawPost;

    /**
     * PostHelper constructor.
     * @param  WP_Post  $rawPost
     */
    public function __construct($rawPost)
    {
        $this->rawPost = $rawPost;
    }

    /**
     * @return array
     */
    public function simple()
    {
        $post = [];
        $post['id'] = $this->rawPost->ID;
        $post['post_name'] = $this->rawPost->post_name;
        $post['post_type'] = $this->rawPost->post_type;
        $post['post_status'] = $this->rawPost->post_status;

        $post['post_title'] = $this->rawPost->post_title;
        $post['post_excerpt'] = $this->rawPost->post_excerpt;
        $post['featured_image'] = get_the_post_thumbnail_url($this->rawPost->ID, 'large');
        $post['post_date_gmt'] = $this->rawPost->post_date_gmt;
        $post['post_modified_gmt'] = $this->rawPost->post_modified_gmt;

        $terms = $this->retrieveTerms();
        $post['post_platforms'] = $this->filterTerms($terms, Constants::platformsCategoryID());
        $post['post_topics'] = $this->filterTerms($terms, Constants::topicsCategoryID());

        $post['post_tags'] = $this->retrieveTags();

        return $post;
    }

    public function withDetails()
    {
        $post = $this->simple();
        $post['post_content'] = $this->getContent();

        return $post;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->rawPost->post_name;
    }

    /**
     * @return mixed|void
     */
    public function getContent()
    {
        return apply_filters('the_content', $this->rawPost->post_content);
    }

    /**
     * @return array<WP_Term>
     */
    private function retrieveTerms()
    {
        $termIDs = wp_get_post_categories($this->rawPost->ID);

        $terms = [];
        foreach ($termIDs as $termID) {
            $terms[] = get_category($termID);
        }

        return $terms;
    }

    private function filterTerms($terms, $parentId)
    {
        $result = [];

        foreach ($terms as $term) {
            if ($term->parent === $parentId) {
                $result[] = $this->normalizeTerm($term);
            }
        }

        return $result;
    }

    /**
     * @param  WP_Term  $term
     * @return array
     */
    private function normalizeTerm($term)
    {
        return [
            'id' => $term->term_id,
            'name' => $term->slug,
            'title' => $term->name,
        ];
    }

    /**
     * @return array
     */
    private function retrieveTags()
    {
        $tags = wp_get_post_tags($this->rawPost->ID);
        $terms = [];

        foreach ($tags as $tag) {
            $terms[] = $this->normalizeTerm($tag);
        }

        return $terms;
    }
}
