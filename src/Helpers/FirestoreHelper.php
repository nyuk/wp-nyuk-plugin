<?php

namespace Nyuk\Helpers;

use GuzzleHttp\Client;

class FirestoreHelper
{
    private $projectId;

    private $clientEmail;

    private $privateKey;

    private $hostName = 'https://firestore-proxier.now.sh/api/';

    public function __construct()
    {
        $this->validateConfiguration();
    }

    private function validateConfiguration()
    {
        $this->projectId = defined('FIRESTORE_PROJECT_ID') ? FIRESTORE_PROJECT_ID : '';
        if ($this->projectId === '') {
            ddd('FIRESTORE_PROJECT_ID is missing');
        }

        $this->clientEmail = defined('FIRESTORE_CLIENT_EMAIL') ? FIRESTORE_CLIENT_EMAIL : '';
        if ($this->clientEmail === '') {
            ddd('FIRESTORE_CLIENT_EMAIL is missing');
        }

        $this->privateKey = defined('FIRESTORE_PRIVATE_KEY') ? FIRESTORE_PRIVATE_KEY : '';
        if ($this->privateKey === '') {
            ddd('FIRESTORE_PRIVATE_KEY is missing');
        }
    }

    /**
     * @param  string  $collection
     * @param  string  $doc
     * @param  array  $data
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setDocument($collection, $doc, $data)
    {
        $client = new Client();

        return $client->post($this->hostName.'/v1/action:SetDocument', [
            'json' => [
                'projectId' => $this->projectId,
                'credentials' => [
                    'client_email' => $this->clientEmail,
                    'private_key' => $this->privateKey,
                ],
                'payload' => [
                    'collection' => $collection,
                    'doc' => ''.$doc,
                    'data' => $data,
                ],
            ],
        ]);
    }

    /**
     * @param  string  $collection
     * @param  string  $doc
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function deleteDocument($collection, $doc)
    {
        $client = new Client();

        return $client->post($this->hostName.'/v1/action:DeleteDocument', [
            'json' => [
                'projectId' => $this->projectId,
                'credentials' => [
                    'client_email' => $this->clientEmail,
                    'private_key' => $this->privateKey,
                ],
                'payload' => [
                    'collection' => $collection,
                    'doc' => ''.$doc,
                ],
            ],
        ]);
    }
}
