<?php

/*
Plugin Name: Nyuk Plugin
Plugin URI: https://gitlab.com/nyuk/wp-nyuk-plugin
Description: Lorem ipsum dolor sit amet
Author: Project Nyuk
Version: 0.0.1
Author URI: https://gitlab.com/nyuk
*/

require __DIR__.'/vendor/autoload.php';

require_once __DIR__.'/functions.php';

require_once __DIR__.'/src/Actions/delete_post.php';
require_once __DIR__.'/src/Actions/rest_api_init.php';
require_once __DIR__.'/src/Actions/save_post.php';

require_once __DIR__.'/src/Filters/sanitize_file_name.php';
